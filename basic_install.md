## Basic enviroment:
tar > v1.26

python3

<br>`Please run below step in non root user`
## Download Yocto:
``` bash
$git clone git://git.yoctoproject.org/poky
$cd poky
$git checkout -b zeus origin/zeus
```

## Initializing the Build Environment:
``` bash
$ source oe-init-build-env mybuilds
```
or
``` bash
$ source oe-init-build-env
```

## Modify conf/local.conf
Please refer [local.conf](local.conf)

## Start create Yocto image
``` bash
bitbake core-image-full-cmdline
```
![install](pic/install.jpg)

## Start up Yocto image
``` bash
runqemu qemux86-64
```

if you execute command in xwindow may see below issue

![x11 issue](pic/x11_issue.jpg)

Please execute ```xhost +``` in root user

then you can see your Yocto is running
![run](pic/start.jpg)
